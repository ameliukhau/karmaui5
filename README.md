## Sample UI5 app to demonstrate Unit (QUnit) and integration (OPA5) tests with Karma runner

**Karma installation**

Node.js required

https://github.com/SAP/karma-ui5

https://open.sap.com/courses/ui52/items/6RL1EG8WNottCe2enSp9KM

1.	Open a shell and go to the folder that contains the extracted app (not webapp folder).

npm install -g karma-cli

2. npm install

3. ui5 init

4. Add the karma config files

Create two files called karma.conf.js and karma-ci.conf.js and edit them

**Execution**

npm run karma-ci

npm ru karma

**RELATED MATERIAL**

•	[Demo Kit: Testing Tutorial](https://ui5.sap.com/#/topic/291c9121e6044ab381e0b51716f97f52)

•	[Demo Kit: Unit Testing with QUnit](https://ui5.sap.com/#/topic/09d145cd86ee4f8e9d08715f1b364c51)

•	[GitHub: Karma plugin for UI5](https://github.com/SAP/karma-ui5)

•	[npmjs: Creating a package.json file](https://docs.npmjs.com/creating-a-package-json-file)

•	[npmjs: Specifying dependencies and devDependencies in a package.json file](https://docs.npmjs.com/specifying-dependencies-and-devdependencies-in-a-package-json-file)

