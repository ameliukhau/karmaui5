/* global module*/

module.exports = function (config) {
	config.set({

		frameworks: ["ui5"],

		logLevel: config.LOG_ERROR,

		loggers: [{
				type: 'console'
			},
			{
				type: 'file',
				filename: 'karma.log',
				maxLogSize: 65536,
				// backups: 3
			}
		],

		junitReporter: {
			outputDir: 'reports', // results will be saved as $outputDir/$browserName.xml 
			outputFile: 'junit/webapp.xml',
			// if included, results will be saved as $outputDir/$browserName/$outputFile 
			suite: 'sapui5', // suite will become the package name attribute in xml testsuite element
			useBrowserName: true // add browser name to report and classes names 
		},

		browsers: [
			'Chrome'
			//		'Chrome_without_security'
			//      'PhantomJS_custom'
		],

		customLaunchers: {
			Chrome_without_security: {
				base: 'Chrome',
				options: {
					viewportSize: {
						width: 1440,
						height: 900
					}
				},
				flags: ['--disable-web-security']
			},

			PhantomJS_custom: {
				base: 'PhantomJS',
				options: {
					viewportSize: {
						width: 1920,
						height: 1080
					}
				}
			}
		}
	});
};